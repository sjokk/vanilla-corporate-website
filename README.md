![Pine logo](Pine.png)
# PINE Corporate Website

This document will provide a brief overview of the project members that are involved 
in creating the website for the company named Pine. It contains a brief project 
description and a url to the live corporate website of Pine.

## Project members

Full name | Student number
--------- | --------------
Miguel Boekhold | 382069
Remy Bos | 2158030
Teodor Georgescu | 3242749

## Project description

This project will touch all elements of the web design process, the first milestone 
will be in the form of a  design document that includes components as: 
target audience information, a site map for the website, the wireframes for 
the different pages, and a visual design of the website.

The target audience will be decribed in detail as to their motivations and 
goals for visiting the website. It will provide the necessary information for 
each specific kind of visitor and describe how often these visitors will visit
the Pine corporate website.

Based on the design document the next steps of the project includes the
creation of html pages for each web page of the web application, the actual 
styling of the pages by means of css, functionality by use of AJAX, and
interaction by JavaScript, and lastly the website will be povided with 
php code to create dynamic webpages.

## Changes made for profile picture display. (EXTRA GRADING ASSIGNMENT)

Changes made to the live website include the clientexample page which you get directed to after succesfully loggin in, a page called clientpicture and a script called upload.php.

After landing at the clientexample page, you will notice a small box in the top right corner that will hold a standard picture if a profile picture has not yet been set (once the profile picture is set, this box will contain a small version of the uploaded picture). 

When clicking on the box (besides the log out field), you will be redirected towards the client picture page, where you will see a default picture when no picture has been set yet. 
Clicking the button to upload a picture results in the picture being checked for validity, as well as the server checking if the picture does not already exist.
In case the picture does not exist and all checks have been passed, the picture will be uploaded to the server.
Once a new picture is selected and uploaded, the old picture will be deleted. (All pictures will be previewed in the large img place holder on the picture upload page.)

Once you decide to go back to the clientexample page, you will see these changes have taken place and the small user profile box in the top right corner of the page.

To view these changes please make use of the link to the live website and use the username / password combinations at the bottom of this readme file.

## Live website

The url below provides a link to the corporate website of Pine.

[Link to the live website.](https://i231896.hera.fhict.nl/)

Another one.

[Link to remy's improvisation.](https://i231896.hera.fhict.nl/remy_improv/index.php?page=home)

To test the client page please make use of the usernames as below:

User name | Password
--------- | ---------
kappa@sama.jp | 1234
remy@pine.bin | 4321