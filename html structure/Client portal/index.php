<?php
	// Obtain session information.
	session_start();
	if (isset($_SESSION['isLegit'])) // If the variable has a value.
	{
		if ($_SESSION['isLegit'] == 1) // Test if the flag is true (succesfull log in).
		{
			header('Location: ../Client portal/clientexample.php');
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Clients of Pine</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="../css/Main.css" type="text/css" rel="stylesheet">
	<link href="../css/Footer.css" type="text/css" rel="stylesheet">
</head>
<body> 
	<div id = "page">
		<div id="content">
			<h1>
				<a href="../index.html"> <img src="../images/pineLogo.PNG" width="600" height="=200"></a>
			</h1>
			
			<div id="contact">
				<form action="../php/login.php" method="POST">
					<fieldset>
						<legend>
						LOG IN
						</legend>
						<p>
							<label for="email">E-mail:</label>
							<input type="email" name="email" id="email" placeholder="Example@gmail.com" maxlength="50" required/>
						</p>
						<p>
							<label for="password">password:</label>
							<input type="password" name="password" placeholder="password" id="password" maxlength="50" required/>
						</p>
						<p>
							<input type="submit" name="Submit" value="Log In"/>
						</p>
					</fieldset>
				</form> 
			</div>
		</div>
	</div>
    <script src="../js/Validation.js"></script>
	<footer>
		<div class="footer">
			<table>
				<thead>
					<tr>
						<th>Get in touch with us</th>
						<th>Quick navigation</th>
						<th>Pages</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>PINE</td>
						<td><a href="../Services/index.html">Services</a></td>
						<td><a href="../About us/index.html">About PINE</a></td>
					</tr>
					<tr>
						<td>Postbus 23</td>
						<td><a href="../About us/index.html">About us</a></td>
						<td><a href="../Contact/index.html">Contact us</a></td>
					</tr>
					<tr>
						<td>5612 MA Eindhoven</td>
						<td><a href="../Contact/index.html">Contact</a></td>
						<td><a href="">Privacy policy</a></td>
					</tr>
					<tr>
						<td>The Netherlands</td>
						<td><a href="index.html">Client portal</a></td>
						<td><a href="">Terms and conditions</a></td>
					</tr>
				</tbody>
			</table>
			<p>&copy; 2018 Pine. All rights reserved.</p>
		</div>
	</footer>
</body>
</html>