// Wrapper function that matches the button.
function routeDescription() {
	getLocation()
}

function calculateAndDisplayRoute(position) {
	directionsService.route({
		origin: {lat: position.coords.latitude, lng: position.coords.longitude},
		destination: {lat: 51.4521956, lng:5.4808936}, // Fontys Campus is destination.
		travelMode: 'DRIVING' // Travel by car (or w/e).
	}, function(response, status) {
		if (status === 'OK') {
			directionsDisplay.setDirections(response); // If successfull response, update display to show begin and destination.
			} else {
				window.alert('Directions request failed due to ' + status);
				}
		});
}

// HTML5 build in function that asks the user permission for it's coordinates.
function getLocation() {
	if (navigator.geolocation) {
		// After successfully obtaining coordinates, pass the position to the calculate and display function.
		navigator.geolocation.getCurrentPosition(calculateAndDisplayRoute);
	} else {
		alert("Geolocation is not supported by this browser.");
	}
}