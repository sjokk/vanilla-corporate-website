function validateFields() {
	// All 3 have to be valid, oherwise return false to not send the form.
	return validateName() && validateEmail() && validateMessage();
}

function validateLogIn() {
	// All 2 inputs have to be valid, otherwise return false to not send the form.
	return validateEmail() && validatePassword();
	
}

function validateName(inputText=document.getElementById("name").value) {
	
	var nameRegexp = /^[A-Za-z\s]+$/;
	
	if (inputText.match(nameRegexp)) {
		document.getElementById("validationText").innerHTML = "";
		return true;
	} else {
		document.getElementById("validationText").innerHTML = "The name you entered is invalid.";
		return false;
	}
}

function validateEmail(inputText=document.getElementById("email").value) {
	var emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
	if (inputText.match(emailRegexp)) {
		document.getElementById("validationText").innerHTML = "";
		return true;
	} else {
		document.getElementById("validationText").innerHTML = "The e-mail address you entered is invalid.";
		return false;
	}
}

function validateMessage(inputText=document.getElementById("message").value) {
	
	if ((inputText == "") || (inputText.includes("Enter your question..."))) {
		document.getElementById("validationText").innerHTML = "The message you entered is invalid.";
		return false;
	}
	document.getElementById("validationText").innerHTML = "";
	return true;
}

function validateSearch(inputText=document.getElementById("search").value, alertMessage = "Please enter a keyword to search.") {
	
	if (inputText == "") {
		document.getElementById("validationText").innerHTML = alertMessage;
		return false;
	}
	document.getElementById("validationText").innerHTML = "";
	return true;
}

function validatePassword(inputText=document.getElementById("password").value) {
	if (inputText == "") {
		document.getElementById("validationText").innerHTML = "The password you entered is invalid.";
		return false;
	}
	else {
		document.getElementById("validationText").innerHTML = "";
		return true;
	}
}