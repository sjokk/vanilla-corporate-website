<?php
	include("../templates/urls.php");
	// Start session to obtain out stored information.
	session_start();
	
	// Destroy session.
	session_unset();
	session_destroy();
	
	// Instant redirect to the home page.
	header("Location: " . $home);
?>