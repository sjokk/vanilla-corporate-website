<?php
	function ConnectToDb()
	{
		// Create a connection to the database.
		$dsn = 'mysql:dbname=dbi231896;host=studmysql01.fhict.local';
		$user = 'dbi231896';
		$password = 'UnsafePassword';
		
		return new PDO($dsn, $user, $password);
	}
	
	// Will serve as the basic function to be reused for all variable queries.
	function QueryDatabaseOnVariable($sql, $associativeArray)
	{
		// Important that SQL input reflects like below and is supplemented by an associative array.
		try
		{
			// Establish a connection to the db.
			$conn = ConnectToDb();
			
			// Prepare the input sequal statement.
			$sth = $conn->prepare($sql);
			
			// Use the supplied name, value pair.
			$sth->execute($associativeArray);
			
			// fetch all the returned rows.
			$result = $sth->fetchAll();
			
			// Close the connection.
			$conn = null;
		}
		catch (PDOException $e)
		{ }
		
		// Return value is a 2-d array.
		return $result;
	}
	
	function ObtainUserInformation($email)
	{
		// Function's goal is to obtain the full record for input email address from db.
		return QueryDatabaseOnVariable('SELECT * FROM signin WHERE email=:email;', [':email'=>$email])[0];
	}
    
function GetUserPersonalinfo($email)
	{
		// Function's goal is to obtain the personal info from a specific user
		return QueryDatabaseOnVariable('SELECT * FROM contactinformation WHERE email=:email;', [':email'=>$email])[0];
	}
	
	// Helper function to validate input email and password.
	function CheckLogIn($email, $password)
	{
		// Goal of this function is to validate if the inserted email address and password combination exists in the db.
		$result = QueryDatabaseOnVariable('SELECT email, password FROM signin WHERE email=:email AND password=:password;', [':email'=>$email, ':password'=>$password]);
		if ($result != null)
		{
			return true;
		}
		return false;
	}
	
	function GetLastLogIn($email)
	{
		// Function's goal is to retrieve last time user logged in.
		$result = QueryDatabaseOnVariable('SELECT lastLogIn FROM lastlogin WHERE email=:email;', [':email'=>$email]);
		if ($result != null)
		{
			$result = $result[0];
		}
		return $result;
	}
	
	function InsertLogIn($email)
	{
		// If no log in datetime exists, this will be the initial one.
		QueryDatabaseOnVariable('INSERT INTO lastlogin (email, lastLogIn) VALUES (:email, :datetime);', [':email'=>$email, ':datetime'=>date("Y-m-d H:i:s")]);
	}
	
	function UpdateLogIn($email)
	{
		// Method to overwrite last login datetime with current one.
		QueryDatabaseOnVariable('UPDATE lastLogIn SET lastlogin=:datetime WHERE email=:email;', [':email'=>$email, ':datetime'=>date("Y-m-d H:i:s")]);
	}
	
	function UpdateProfilePicture($email, $pathToPicture)
	{
		QueryDatabaseOnVariable('UPDATE signin SET profilepicture=:profilepicture WHERE email=:email;', [':email'=>$email, ':profilepicture'=>$pathToPicture]);
	}
	
	function ObtainProfilePicture($email)
	{
		$result = QueryDatabaseOnVariable('SELECT profilepicture FROM signin WHERE email=:email;', [':email'=>$email]);
		if ($result != null)
		{
			$result = $result[0];
		}
		return $result["profilepicture"];
	}
	
	function ObtainCustomerDetails($email)
	{
		// Do not understand why this fucntion returns the values twice?
		return QueryDatabaseOnVariable('SELECT * FROM contactinformation WHERE email=:email;', [':email'=>$email])[0];
	}
	
	function ObtainCompanyName($companyId)
	{
		// Goal of this function is to return the name of the company for that specific user.
		$result = QueryDatabaseOnVariable('SELECT companyName FROM company WHERE idCompany=:idCompany;', [':idCompany'=>$companyId]);
		$result = $result[0];
		
		return $result["companyName"];
	}
	
	function ObtainProjects($companyId)
	{
		// Goal of this function is to obtain the top 3 most recent projects (just imagine as if they are still going).
		$result = QueryDatabaseOnVariable('SELECT * FROM companyproject WHERE idCompany=:companyId LIMIT 3;', [':companyId'=>$companyId]);

		return $result;
	}
	

?>