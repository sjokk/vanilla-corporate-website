<?php
	// Include the db script file for it's functions.
	include('database_methods.php');

	$email = $_POST['email'];
	$password = $_POST['password'];
	
	// Returns a boolean, whether the email and password combination exists.
	$result = CheckLogIn($email, $password);
	
	if ($result)
	{
		// If the login is verified, we can route the user to his/her client page.
		// To keep track of specific details about the user we create a session (object?).
		session_start();
		// Use helper function to obtain user's information from the db.
		$userInformation = ObtainUserInformation($email);
        $personInformation = GetUserPersonalinfo($email);
		
		// Assign variables to keep track of user details without querying the db.
		$_SESSION["firstName"] = $userInformation[0];
		$_SESSION["username"] = $userInformation[1];
		$_SESSION["companyId"] = $userInformation[3];
        $_SESSION["email"] = $email;
        $_SESSION["phone"] = $personInformation[1];
		
		// Flag to use on the index page of the client portal, to omit the login if user was already logged in.
		$_SESSION["isLegit"] = $result;
		
		// Show the user the log in was successful.
		echo "<h1>Valid log in</h1>";
		// Query the db to see if the user already has an existing log in.
		$lastLogIn = GetLastLogIn($email);
		if ($lastLogIn != null)
		{
			// Return a specific welcome message in case user already logged in once before.
			echo "<p>Welcome back " . $_SESSION["firstName"] . ", we missed you!</p>";
			
			// Show last log in.
			echo "<p>Your last log in was at: " . $lastLogIn[0] . ".</p>";
			
			// Update last log in of user to the time s/he just logged in.
			UpdateLogIn($email);
		}
		else
		{
			// Greet user by firstname that's stored in the database.
			echo "<p>Welcome to the client portal, " . $_SESSION["firstName"] .".</p>";
			
			// Insert current log in datetime into the database.
			InsertLogIn($email);
		}
		/* The user that logs in is ficticiously part of a company.
		   Based on that company id, we provide specific content for that user.
		   This should for example include different offers, orders, invoices, projects. */
		
		// Redirect user to the client page after 3 seconds of waiting.
		header("refresh:3; url=../Client portal/clientexample.php");
	}
	else
	{
		// Tell user their log in failed and s/he will be redirected to the log in page.
		// To try again. (vulnerable though)
		echo "<h1>Your credentials are not valid.</h1>";
		echo "<p>You will be redirected shortly ...</p>";
		header("refresh:3; url=../Client portal/index.php");
	}
?>