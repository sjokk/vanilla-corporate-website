<?php
	$userName = $userEmail = $userMessage = "";
	$nameError = $emailError = $messageError = false;

	
	// Add a timestamp to when the message was posted.
	$timeStamp = getdate(); // Is actually an array object containing key-value pairs.
	
	// Validate the variables if the method (POST) was not altered in the form.
	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		// Strip the input for name if it is not an empty string.
		if (!empty($_POST['name'])) // Is required, must contain letters and whitespace.
		{
			$userName = process_input($_POST['name']);
			if (!preg_match("/^[a-zA-Z ]*$/", $userName))
			{
				// If the name does not match any letter and white spacing then flag our variable to true.
				$nameError = true;
			}
		}
		else
		{
			$nameError = true;
		}
		if (!empty($_POST['email'])) // Is required, must contain an @ sign and a .
		{
			$userEmail = process_input($_POST['email']);
			if (!filter_var($userEmail, FILTER_VALIDATE_EMAIL))
			{
				$emailError = true;
			}
		}
		else
		{
			$emailError = true;
		}
		if (!empty($_POST['message'])) // Must not be empty.
		{
			$userMessage = process_input($_POST['message']);
		}
		else
		{
			$messageError = true;
		}
		if (!$nameError && !$emailError && !$messageError)
		{
			mail('remy.bos@student.fontys.nl', "Contact form message from: $userName", "$userMessage", "From: $userEmail");
			echo "<h1>Thank you for your message!</h1>";
			echo "<p>You will be redirected shortly ...</p>";
			header("refresh:3; url=../Contact/index.html");
		}
		else
		{
			echo "<h1>Your input was invalid, redirecting you to the contact page.</h1>";
			header("refresh:3; url=../Contact/index.html");
		}
	}
	
	// Helper method for validating and ensuring nothing malicious can happen.
	function process_input($input)
	{
		$input = trim($input); // Remove excessive white spacing such as space, tab and newline characters.
		$input = stripslashes($input); // Remove backslash characters in the input.
		$input = htmlspecialchars($input); // Convert the input to html escaped characters.
		
		return $input;
	}
?>