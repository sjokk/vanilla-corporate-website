<?php
	session_start();
	// Take out /html structure/ on the live server.
	$user_dir = "/Client portal/profile_pictures/" . $_SESSION["username"] . "/";
	
	// Use the root directory and extend this with the folder + path to our picture folder.
	$target_dir = $_SERVER['DOCUMENT_ROOT'] . $user_dir;
	
	// Extend the path with the created directory if it does not yet exist.
	if (!file_exists($target_dir))
	{
		mkdir($target_dir);
	}
	
	// Seperate the file name from the rest so we can use this later on to store in the db.
	$file_name = basename($_FILES["fileToUpload"]["name"]);
	$target_file = $target_dir . $file_name;
	
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
		$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		if($check !== false) {
			echo "File is an image - " . $check["mime"] . ".";
			$uploadOk = 1;
		} else {
			echo "File is not an image.";
			$uploadOk = 0;
		}
	}
	
	// Check if file already exists
	if (file_exists($target_file)) {
		echo "Sorry, file already exists.";
		$uploadOk = 0;
	}
	
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 500000) {
		echo "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
		echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
	}
	
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		echo "Sorry, your file was not uploaded.";
	
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		} else {
			echo "Sorry, there was an error uploading your file.";
			echo $target_file;
		}
	}
	
	if ($uploadOk)
	{
		// Remove the old picture then store the new one.
		include("database_methods.php");
		
		$picture = ObtainProfilePicture($_SESSION["username"]);
		$oldPicture = $_SERVER['DOCUMENT_ROOT'] . $picture;
		
		// Test if specific variable is a file.
		if (is_file($oldPicture))
		{
			unlink($oldPicture);
		}
		
		// Update the db to the hold the path to the new picture.
		UpdateProfilePicture($_SESSION["username"], $user_dir . $file_name);
	}
	header("refresh:3; url=../Client portal/clientpicture.php");
?>