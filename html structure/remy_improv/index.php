<!DOCTYPE html>
<html>
	<?php
		// Obtain session information.
		session_start();
		
		// Include all the absolute urls that are in one file.
		include("templates/urls.php");
		
		$navbar = "templates/navbar.php";
		$header = "templates/header.php";
		
		// Collect the value that the url query page points to.
		if (isset($_GET['page']))
		{
			$page = $_GET['page'];
		}
		else // Otherwise point to the home page.
		{
			$page = "home";
		}
		switch ($page)
		{
			case "home":
			case "index":
			$title = "Welcome to Pine!";
			include($navbar); 
			include("index.html");
			break;
			
			case "services":
			case "Services":
			$title = "Services";
			include($navbar);
			include("Services/index.html");
			break;
			
			case "webdesign":
			$title = "Web Design";
			include($navbar);
			include("Services/Web design/index.html");
			break;
			
			case "about":
			$title = "About Pine";
			include($navbar);
			include("About us/index.html");
			break;
			
			case "contact":
			$title = "Contact Pine";
			include($navbar);
			include("Contact/index.html");
			break;
			
			case "client":
			if (isset($_SESSION['isLegit'])) // If the variable has a value.
			{
				if ($_SESSION['isLegit'] == 1) // Test if the flag is true (succesfull log in).
				{
					header('Location: ' . $clientportal . '');
				}
			}
			$title = "Clients of Pine";
			include("Client portal/index.html");
			include($header);
			break;
			
			case "clientportal":
			if (isset($_SESSION['isLegit']))
			{
				if ($_SESSION['isLegit'] == 1)
				{
					$title = "Client Portal";
					include("Client portal/clientexample.html");
					include($header);
					break;
				}
			}
			else
			{
				header("Location: " . $home);
			}
			
			default:
			header("Location: " . $home);
			break;
		}
		
		include ("templates/footer.php"); 
	?>	
</body>
</html>