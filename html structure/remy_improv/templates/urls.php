<?php
	$root = "/html structure/";
	$base = $root . "remy_improv/";
	
	$home = $base . "index.php?page=home";
	
	// Pages navbar and footer
	$services = $base . "index.php?page=services";
	$webDesign = $base . "index.php?page=webdesign";
	$about = $base . "index.php?page=about";
	$contact = $base . "index.php?page=contact";
	$client = $base . "index.php?page=client";
	$clientportal = $base . "index.php?page=clientportal";
	
	// Pictures home page.
	$_1 = $root . "images/1.PNG";
	$_2 = $root . "images/2.PNG";
	$_3 = $root . "images/3.PNG";
	$_4 = $root . "images/4.PNG";
	
	// CSS
	$mainStyle = $root . "css/Main.css";
	$footerStyle = $root . "css/Footer.css";
	$webdesignStyle = $root. "css/Webdesign.css";
	$contactStyle = $root . "css/Contact.css";
	$clientExampleStyle = "css/Clientexample.css";
	
	// Pictures navigation bar.
	$imgPine = $root . "images/justpineLogo.PNG";
	$imgHome = $root . "images/homeBTN.PNG";
	$imgPineLarge = $root . "images/pineLogo.PNG";
	
	// Specific services.
	$backEnd = $webDesign;
	$appDevelopment = $webDesign;
	$databaseDevelopment = $webDesign;
	
	// Pictures services page.
	$imgBackEnd = $root . "images/Back-end.png";
	$imgWebDesign = $root . "images/webdesign.jpeg";
	$imgAppDevelopment = $root . "images/AppDevolpment.png";
	$imgDatabaseDevelopment = $root . "images/database.png";
	
	// Pictures webdesign page.
	$imgWebDesignBanner = $root . "images/webdesign.jpeg";
	$imgMobileWebApp = $root . "images/mobilewebapp.jpg";
	$imgImacWebsite = $root . "images/imacwebsite.jpg";
	
	// Client information (not implemented yet though).
	$offers = $base . "Client portal/Client Information/offers.html";
	$orders = $base . "Client portal/Client Information/orders.html";
	$invoices = $base . "Client portal/Client Information/invoices.html";
	$projects = $base . "Client portal/Client Information/projects.html";
	
	// Javascripts.
	$validation = $root . "js/Validation.js";
	$routeDescription = $root . "js/RouteDescription.js";
	$mapAPIkey = "AIzaSyDtP86wK65ZXDe-g3xDxo5oPAxR7yULYQo";
	
	// Php scripts
	$search = $root . "php/search_result.php";
	$sendForm = $root . "php/send_message.php";
	$login = $root . "php/_login.php";
	$logout = $root . "php/_logout.php";
?>