<?php
	include('urls.php');
	
	echo '<footer>
		<div class="footer">
			<table>
				<thead>
					<tr>
						<th>Get in touch with us</th>
						<th>Quick navigation</th>
						<th>Pages</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>PINE</td>
						<td><a href=" '. $services .' ">Services</a></td>
						<td><a href="'. $about .'">About PINE</a></td>
					</tr>
					<tr>
						<td>Postbus 23</td>
						<td><a href="'. $about .'">About us</a></td>
						<td><a href="'. $contact .'">Contact us</a></td>
					</tr>
					<tr>
						<td>5612 MA Eindhoven</td>
						<td><a href="'. $contact .'">Contact</a></td>
						<td><a href="">Privacy policy</a></td>
					</tr>
					<tr>
						<td>The Netherlands</td>
						<td><a href="'. $client .'">Client portal</a></td>
						<td><a href="">Terms and conditions</a></td>
					</tr>
				</tbody>
			</table>
			<p>&copy; 2018 Pine. All rights reserved.</p>
		</div>
	</footer>'; 
?>